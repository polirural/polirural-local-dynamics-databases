# Polirural Local Dynamics Databases

This repository contains system dynamics models (SDM) that have been developed through the Polirural project (EU/H2020)

What follows is a brief description of the PoliRural Base Qualitative Model. Additionally, a reference of the works to produce Local Customized Model has also been introduced. Together they constitute the basis of the prototype and the GIT repository.

## PoliRural Base Qualitative Model
The Base Qualitative Model is PoliRural SDM ed. 3, and it has been evolving from the first edition. The idea of the model is to be a general template from where to adapt to local data and dynamics.

The model is made up of eight modules: POPULATION, EDUCATION, QUALITY OF LIFE, AGRICULTURE, NATURAL CAPITAL, EMPLOYMENT, RURAL ATTRACTIVENESS and RURAL RETENTION CAPACITY. They are highly integrated with each other as shown in the image below, and together they reproduce the main dynamics of a generic rural area in Europe. Deliverable D5.3 contains a detailed description of the model.

The main challenge when building the model has been to find an equilibrium between relevance, usefulness and resources availability. The result is a complete working model in which the value is not so much in the loops described but in putting together single relations between variables.

Then there is a first value as a visual map of relations. From there, in a further step, the model contains a global consideration of known and quantified facts or relations, and some others also known but not quantifiable. In this sense the value comes from putting all known relations and facts in a single model.

Complexity is thus tackled partially. The model reflects the complexity of multiple relations and agents acting iteratively over time. At this level complexity is considered. But on the other hand, complex systems are highly dependent on initial conditions. This mathematical complexity is not reflected in the model. This would require an in depth analysis in each of the pilot areas and a calibration effort out of reach in the scope of the project. Leaving aside considerations about uncertainty and complexity.

The discussion about the usefulness of the exercise follows in 3.2, dealing with SDM in the context of foresight.

## Local Customized Model
Local customized models objective is to develop the model further on and get more accurate results and interpretations of local reality and perspectives, as stated in D3.3.

In this sense, local customized models may have the form of new modules or new dynamics added to existing ones. The origin may be a particularly dominant local dynamic not observed in the template, or a strong impact to consider in the scenarios for the future.

These pieces of model (local rural dynamics) can be expressed in SD feedback loops or even in causal loop diagrams (CLD).

Whatever is the case, the prototype includes a procedure to introduce them into the GIT repository, conveniently tagged and classified, and with a user interface so that it becomes a source of knowledge, comparison and best practices for rural communities.
